Ámbar
=================

Ámbar is a complete project who integrates Raspberry Pi, Arduino, and a NodeJS API to track the humedity and temeperature of something on a distributed system.

How does it works

An Arduino with a DHT22 sensor detects the data we need and transport them via Serial to the Raspberry Pi, then the Raspberry Pi consumes the NodeJS API(preferably on a internet Server) and when the data is on the web you can distribute it as you like.


Installation
=================

Arduino
-----------------
Concect the DHT22 Sensors like this
![alt tag](https://plot.ly/static/img/workshop/arduino-dht22-temperature-hookup.svg)

Use a the DHT library and load the script contained in Arduino folder.Raspberry Pi

Raspberry Pi
-----------------

First of all you'll need a Raspberry Pi with an installed OS, then make sure you have python-serial and requests libraries to continiue.

Then you'll need to load the scripts which autimatic detects all the arduino devices conected to the Raspberry Pi USB ports.

The simplest and not Asyncronnous API with Django
-----------------

Install Django Rest Framework and run the django development server with:

```python
python manage.py runserver 0.0.0.0:8000
```

Change the 'url' variable on the getData.py file that is loaded on the Raspberry Pi to localhost url or whatever you will implamente and enjoy the software.

API Reference
=================

Django (the simplest server)
-----------------

You only have two endpoints:

```
http://theHostYouUse.com/api/post/arduino/
```
Which recieves
```
{
	"serialNumber": "safdgdshsdfas-fshjdfhga-stsrywtw4ar43t5wae",
}
```

And
```
http://theHostYouUse.com/api/post/record/
```
Which recieves
```
{
	"arduino": "safdgdshsdfas-fshjdfhga-stsrywtw4ar43t5wae",
	"temperature": "20",
	"humidity": "59",
}
```


Contributors
=================

Pablo Trinidad - [github](https://github.com/pablotrinidad "Title").

Eder Vázques - [github](https://github.com/EderVs "Title").

César Lara - [github](https://github.com/pablotrinidad "Title").

Rafa Miranda - [github](https://github.com/RafaMirandaVi "Title").


License
=================

You can use this content as you wish, <3 Open Source <3