from django.contrib import admin
from models import Arduino, Record

@admin.register(Arduino)
class ArduinoAdmin(admin.ModelAdmin):
    list_display = ('id', 'serialNumber')
    
@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    list_display = ('id', 'arduino', 'temperature', 'humidity', 'registerTime')
    list_filter = ('arduino',)