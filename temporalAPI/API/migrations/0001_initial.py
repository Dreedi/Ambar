# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Arduino',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('serialNumber', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Registro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('temperature', models.CharField(max_length=5)),
                ('humidity', models.CharField(max_length=5)),
                ('registerTime', models.DateTimeField(auto_now=True)),
                ('arduino', models.ForeignKey(to='API.Arduino')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
