# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Record',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('temperature', models.CharField(max_length=5)),
                ('humidity', models.CharField(max_length=5)),
                ('registerTime', models.DateTimeField(auto_now=True)),
                ('arduino', models.ForeignKey(to='API.Arduino')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='registro',
            name='arduino',
        ),
        migrations.DeleteModel(
            name='Registro',
        ),
    ]
