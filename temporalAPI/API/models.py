from django.db import models

# Create your models here.
class Arduino(models.Model):
    serialNumber = models.CharField(max_length=255, unique=True)
    
    def __unicode__(self):
        return self.serialNumber
        
class Record(models.Model):
    arduino = models.ForeignKey(Arduino)
    temperature = models.CharField(max_length=5)
    humidity = models.CharField(max_length=5)
    registerTime = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        text = str(self.arduino) + " - " + self.temperature + " - " + self.humidity
        return text