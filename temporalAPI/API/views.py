from django.http import HttpResponse

from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt

from models import Arduino, Record

from datetime import datetime
# Create your views here.
def home(request):
    arduino = Arduino.objects.all()[0]
    recordsByArduino = Record.objects.filter(arduino=arduino)[:]
    diccionario = {"records": recordsByArduino}
    return render(request, "index.html", diccionario)
    
@csrf_exempt
def addArduino(request):
    newArduino = Arduino()
    if request.method == 'POST':
        serialNumber = request.POST.get('serialNumber','')
        newArduino.serialNumber = serialNumber
        newArduino.save()
        return HttpResponse("Realized")
    
    return HttpResponse("Without POST Method")
    
@csrf_exempt
def addRecord(request):
    newRecord = Record()
    if request.method == 'POST':
        arduino = request.POST.get('arduino','')
        goodArduino = Arduino()
        try:
            goodArduino = Arduino.objects.get(serialNumber__exact=arduino)
        except:
            goodArduino = Arduino(serialNumber=arduino)
            goodArduino.save()
        temperature = request.POST.get('temperature','')
        humidity = request.POST.get('humidity','')
        newRecord.arduino = goodArduino
        newRecord.temperature = temperature
        newRecord.humidity = humidity
        newRecord.save()
        return HttpResponse("Realized")
    
    return HttpResponse("Without POST Method")