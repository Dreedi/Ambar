from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'API.views.home', name='home'),
    
    url(r'^api/post/arduino', 'API.views.addArduino', name="addArduino"),
    url(r'^api/post/record', 'API.views.addRecord', name="addRecord"),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
