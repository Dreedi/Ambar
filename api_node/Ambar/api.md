#API REST of Ambar

Ámbar is a complete project who integrates Raspberry Pi, Arduino, and a NodeJS API to track the humedity and temeperature of something on a distributed system.

Url : ambar.dreedi.com/api/

##Create user [POST]
    Request [POST] /users/
    {
        user:
        {
            username: "Dreedi",
            email: "ambar@dreedi.com",
            password: "password",
            name: "Ambar"
        }
    }

    Response
    {
        user:
        {
            _id : ObjectId,
            username: "Dreedi",
            email: "ambar@dreedi.com",
            password: "password",
            name: "Ambar"
        }
    }

##Get users by id [GET]
    Request GET /user/<id>
    
    Response
    {
        user:
        {
            _id : <id>,
            username: "<id username>",
            email: "<id email>",
            password: "<id password>",
            name: "<id name>"
        }
    }

##User Login
    Request POST /login/
    {
        user:
        {
            email: "<email>",
            password: "<password>"
        }
    }

    Response
    if all correct
    {
        id : "<user id>",
        response: "Success",
        res : true
    }
    if password wrong
    {
        response:"Invalid Password",
        res:false
    }
    else
    {
        response:"User not exist",
        res:false
    }

##Delete user
    Request DELETE /user/<id>
  
##Create collection
    Request POST /collections/
    {
        collection: 
        {
            name: "<name>"
        }
    }
    
    Response
    {
        collection
        {
            name: "<name>"
        }
    }

##Get collection by id
    Request GET /collection_by_id/<id>
    
    Response
    {
        collection: 
        {
            _id: "<id>",
            name: "<id name>",
            arduinos: [<id arduinos>]
        }
    }

##Get all collections
    Request GET /collections/
    
    Response
    {
        collections:
        [{
            _id: "<id>",
            name: "<id name>",
            arduinos: [<id arduinos>]
        }]
    }

##Search collections
    Request GET /collection/search/<search>
        
    Response
    {
        collections:
        [{
            _id: "<id>",
            name: "<id name>",
            arduinos: [{arduino}]
        }]
    }

##Delete collection
    Request DELETE /collection/<id>

##Create arduino
    Request POST /arduinos/
    {
        arduino:
        {
            id_arduino: "<id_arduino>",
            collection: "<collection id>"
        }
    }
    
    Response
    {
        arduino: 
        {
            collections : "<collection id>",
            id_arduino : "<id_arduino>"
        }
    }

##Get arduino by id
    Request GET /arduino_by_id/<id>

    Response
    {
        arduino: 
        {
            id_Arduino: "<id_arduino>",
            collections: [{collection}],
            RegistryNumber : "<arduino registry number>",
            pieces : [{piece}],
            temperatures : [{
                id : "<temperature id>",
                temperature : <temperature number>,
                time : <time number>
            }],
            humidities : [{
                id : "<humidity id>",
                temperature : <humidity number>,
                time : <time number>
            }],
            criticTemperature : {
                minTemperature : <temperature number>,
                maxTemperature : <temperature number>
            },
            criticHumidity : {
                minHumidity : <humidity number>,
                maxHumidity : <humidity number>
            },
            responsableUsers : [{user}]
        }
    }

##Get all arduinos
    Request GET /arduinos/
    
    Response
    {
        arduino: 
        {
            id_Arduino: "<id_arduino>",
            collections: [{collection}],
            RegistryNumber : "<arduino registry number>",
            pieces : [{piece}],
            temperatures : [{
                id : "<temperature id>",
                temperature : <temperature number>,
                time : <time number>
            }],
            humidities : [{
                id : "<humidity id>",
                temperature : <humidity number>,
                time : <time number>
            }],
            criticTemperature : {
                minTemperature : <temperature number>,
                maxTemperature : <temperature number>
            },
            criticHumidity : {
                minHumidity : <humidity number>,
                maxHumidity : <humidity number>
            },
            responsableUsers : [{user}]
        }
    }

##Add collection in arduino
    Request PUT /arduino_collection/
    {
        arduinoId: "<arduino id>",
        collectionId: "<collection id>"
    }
    
    Response
    {
        arduino: 
        {
            collections : [{collection}]
        }
    }

##Add responsable user
    Request PUT /arduino_user/
    {
        arduinoId: "<arduino id>",
        userId: "<user id>"
    }
    
    Response
    {
        arduino: 
        {
            responsableUser : [{user}]
        }
    }

##Create piece
    Request POST /pieces/
    {
        piece: 
        {
            name: "<piece name>",
            registryId: "<piece registry id>"
        }
    }
    
    Response
    {
        piece: 
        {
            name : "<piece name>",
            registryId : "<piece registry id>"
        }
    }

##Get piece by id
    Request GET /piece_by_id/<id>
    
    Response
    {
        piece
        {
            name : "<piece name>",
            registryId : "<piece registry id>",
            arduino : {arduino}
        }
    }

##Get all pieces
    Request GET /pieces/
    
    Response
    {
        piece
        {
            name : "<piece name>",
            registryId : "<piece registry id>",
            arduino : {arduino}
        }
    }

##Add pieces in arduinos
    Request PUT /arduino_piece/
    {
        arduinoId: "<arduino id>",
        pieceId: "<piece id>"
    }

    Response
    {
        arduino: 
        {
            pieces : [{piece}]
        },
        piece: 
        {
            arduino : [{arduino}]
        }
    }

##Delete piece
    Request DELETE /piece/<id>

##Add humidity and temperature
    Request POST /arduinos/record/
    {
        arduino:
        {
            id_arduino: "<arduino id>",
            humidity: <humidity number>,
            temperature: <temperature number>
        }
    }
    
    Response
    {
        temperature: 
        {
            temperature : <temperature number>,
            time : <temperature time>
        },
        humidity: 
        {
            humidity : <humidity number>,
            time: <humidity time>
        }
    }

