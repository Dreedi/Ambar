/**
	Requires
**/
var express = require('express');
var cors = require('cors');
var db = require('./lib/pieces/model');
var path = require('path');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

/**
	Middlewares
**/
app.use(cors());

/**
	Settings
**/
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

/**
	Routes
**/
var api = require('./routes/api');
var app_restorer = require('./routes/app');

app.use('/api/', api);
app.use('/app/' , app_restorer);

/**
	Error handlers
**/
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

//Production
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

/**
	Listening
**/
server.listen(3000, function (){
	console.log('Listening in localhost:3000');
})

exports.io = io;