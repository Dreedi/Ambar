users
{
    "_id" : objectId("123"),
    "username" : "EderVs",
    "email" : "eder.vs.04@gmail.com",
    "password" : "password",
    "name" : "Eder"
}

pieces:
{
    "-id" : objectId("1")
    "name" : "Cabeza Quetzalcoatl",
    rooms: [{
        "name" : "Temporales"
    }],
    "RegistryNumber" : "fdjapijfdpa121",
    "arduino" : "arduino pieza 3"
    "temperatures" : [1, 2, 3],
    "humidities" : [1, 2, 5],
    "criticTemperature" :   {
        "minTemperature" : 0,
        "maxTemperature" : 10
    },
    "criticHumidity" :   {
        "minHumidity" : 0,
        "maxHumidity" : 30
    },
    "responsableUser" : [123]  
}

temperatures:
{
    "_id" : objectId("1"),
    "temperature" : 21.23,
    "date" : "7/11/2014",
    "hour" : "12:17:35"
}

humidities:
{
    "_id" : objectId("1"),
    "humidity" : 21.23,
    "date" : "7/11/2014",
    "hour" : "12:17:35"
}