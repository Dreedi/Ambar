
var User = require('../lib/users/model');

exports.login = function (email,password,callback){
    User.findOne({email: email},function(err,user){
        if(err)
        {
            callback({'response':"Database Error",'res':false});
        }

        if(user)
        {
            var passwordNow = user.password
            var idNow = user._id
            if (passwordNow == password)
            {
                callback({'id' : idNow, 'response': "Success", "res" : true});
            }
            else
            {
                callback({'response':"Invalid Password",'res':false});
            }
        }
        else
        {
            callback({'response':"User not exist",'res':false});
        }
    });
};