var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.createConnection('mongodb://localhost/ambar_demo');

var UserSchema = new Schema({
	username: {type: 'string', unique: true, require: true},
	email: {type: 'string', require: true, unique: true},
	password: {type: 'string', require: true},
	name: {type: 'string', require: true},
	arduinos: [{type: Schema.Types.ObjectId, ref: 'arduino'}],
	phones: ['string'],
	surname: {type: 'string', require: true},
	type: {type: 'number', require: true},
	time : {type : 'Number', require: true},
	activated : {type : 'Boolean', require: true},
});

UserSchema.virtual('id').get(function (){
	return this._id.toHexString();
});

UserSchema.set('toObject', {
	virtuals: true
});

var model = mongoose.model('user', UserSchema);
module.exports = model;