var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.createConnection('mongodb://localhost/ambar_demo');

var CollectionSchema = new Schema({
	name : {type: 'String', require: true, unique: true},
	arduinos : [{ type: Schema.Types.ObjectId, ref: 'arduino' }],
});

var model = mongoose.model('collection', CollectionSchema);
module.exports = model;