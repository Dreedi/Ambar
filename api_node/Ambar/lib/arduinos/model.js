var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.createConnection('mongodb://localhost/ambar_demo');

var ArduinoSchema = new Schema({

	collections : [{ type: Schema.Types.ObjectId, ref: 'collection' }],

	RegistryNumber : {type: 'String'},

	id_arduino : {type: 'String', require: true, unique: true},

	pieces : [{type: Schema.Types.ObjectId, ref: 'piece'}],

	temperatures : [{
		id : {type : Schema.Types.ObjectId},
		temperature : {type : 'Number'},
		time : {type : 'Number'}
	}],

	humidities : [{
		id : {type : Schema.Types.ObjectId},
		humidity : {type : 'Number'},
		time : {type : 'Number'}
	}],

	criticTemperature : {
		minTemperature : {type : 'Number'},
		maxTemperature : {type : 'Number'}
	},

	criticHumidity : {
		minHumidity : {type : 'Number'},
		maxHumidity : {type : 'Number'}
	},

	responsableUsers : [{ type: Schema.Types.ObjectId, ref: 'user' }]
});

var model = mongoose.model('arduino', ArduinoSchema);
module.exports = model;
