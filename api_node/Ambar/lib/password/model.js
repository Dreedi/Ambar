var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/ambar_demo');

var passwordSchema = new Schema({
	password : {type: 'String', require: true},
	type: {type: 'String', require: true}
});

var model = mongoose.model('password', passwordSchema);
module.exports = model;
