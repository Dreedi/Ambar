var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.createConnection('mongodb://localhost/ambar_demo');

var PieceSchema = new Schema({
	name : {type: 'String', require: true},

	registryId : {type: 'String'},

	arduino : { type: Schema.Types.ObjectId, ref: 'collection' }
});

var model = mongoose.model('piece', PieceSchema);
module.exports = model;
