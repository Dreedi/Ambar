/**
	Requires
**/
var express = require('express');
var app = express();
var async = require('async');
var bodyParser = require('body-parser');
var request = require('request');

var passport = require('passport');
var expressSession = require('express-session');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');

var crypto = require('crypto')

var login = require('../Login/loggin')
/**
	Middleware
**/
app.use(expressSession({secret: 'I really love Ambar <3'}));
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.urlencoded({ extended: false }))
app.use(flash());

/**
	DB
**/

var User = require('../lib/users/model');
var Piece = require('../lib/pieces/model');
var Collection = require('../lib/collections/model');
var Arduino = require('../lib/arduinos/model');

/**
	passport
**/
passport.serializeUser(function(user, done) {
  done(null, user._id);
});
 
passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

function encript(user, pass) {
   // usamos el metodo CreateHmac y le pasamos el parametro user y actualizamos el hash con la password
   var hmac = crypto.createHmac('sha1', user).update(pass).digest('hex')
   return hmac
}

var isValidPassword = function(user, password){
  var NewPassword = encript(user.username, password);
  return (NewPassword == user.password);
}

passport.use('login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback : true
  },
  function(req, username, password, done) { 
    // check in mongo if a user with username exists or not
    User.findOne({ 'username' :  username }, 
      function(err, user) {
        // In case of any error, return using the done method
        if (err){
          return done(err);
        }
        // Username does not exist, log error & redirect back
        if (!user){
          return done(null, false, 
                req.flash('message', 'User Not found.'));                 
        }
        // User exists but wrong password, log the error 
        if (!isValidPassword(user, password)){
          return done(null, false, 
              req.flash('message', 'Invalid Password'));
        }
        if (!(user.activated)) {
          return done(null, false, 
              req.flash('message', 'No activated')); 
        }

        // User and password both match, return user from 
        // done method which will be treated like success
        return done(null, user);
      }
    );
}));

/**
	Authenticated
**/

var is_authenticated = function (req, res, next) {
  if (req.isAuthenticated() && req.user.activated)
    return next();
  res.redirect('/app/login/');
};

var is_not_authenticated = function (req, res, next) {
  if ( typeof(req.user) != "undefined") {
    if (!(req.user.activated)) {
      req.logout();
    }
  }
  if (!(req.isAuthenticated()))
    return next();
  res.redirect('/app/');
};

var is_admin_authenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    if (req.user.type == 0) {
      return next();
    }
  }
  res.redirect('/app/');
}

/**
	Routes
**/

var url_api = 'http://localhost:3000/api/'


//GET Login BOTH
app.get('/login/', is_not_authenticated, function (req, res){
	res.render('login', {message: ""});
});


//POST Login BOTH
app.post('/login/',
  passport.authenticate('login', { successRedirect: '/app/',
                                   failureRedirect: '/app/login/',
                                   failureFlash: true })
);


//GET Logout BOTH
app.get('/logout/', function(req, res) {
    req.logout();
    res.redirect('/app/login');
});


//GET Overview BOTH
app.get('/', is_authenticated, function (req, res){

  context = {
    user: req.user,
    admin: (req.user.type == 0),
  }

	res.render('overview', context);
});


/**
  User
**/
//GET Profile BOTH
app.get('/profile/', is_authenticated, function (req, res) {
  request(
    { method: 'GET',
      uri: url_api + 'user/' + req.user._id,
      gzip: true
    }
  , function (error, response, body) {
      if (error)
      {
        return res.send('Error');
      }

      context = {
        user: (JSON.parse(body).user),
        admin: (req.user.type == 0),
      }

      res.render('profile', context);
    }
  );
});


//GET All Users ADMIN
app.get('/users/', is_admin_authenticated, function (req, res) {
  
  var status = req.param('stu');
  var only = req.param('only');
  var admins = true;
  var rest = true;
  var message = '';

  if (parseInt(status) == 23) {
    message = 'Usuario borrado exitosamente.';
  }

  if (only == 'admins') {
    rest = false;
  }

  if (only == 'rest') {
    admins = false;
  }

  request({
    method: 'GET',
    uri: url_api + 'user/',
    gzip: true
  }, function (error, response, body) {
      if (error) {
        return res.send('Error');
      }

      var users = JSON.parse(body).users
      var users_to_show = [];

      if (!(admins)) {
        for (var i = 0; i < users.length; i++) {
          if (users[i].type != 0) {
            users_to_show.push(users[i])
          }
        }
      }

      if (!(rest)) {
        for (var i = 0; i < users.length; i++) {
          if (users[i].type != 1) {
            users_to_show.push(users[i]);
          }
        }
      }

      if (rest && admins) {
        users_to_show = users;
      }



      for (var i = 0; i < users_to_show.length; i++) {
        var first_day = new Date(users_to_show[i].time);
        users_to_show[i].first_day = first_day.getHours() + ":" + first_day.getMinutes() + " | " + first_day.getDate() + "/" + first_day.getMonth() + "/" + first_day.getFullYear()
      }

      context = {
        users: users_to_show,
        user: req.user,
        admin: (req.user.type == 0),
        message: message,
      }

      res.render('users', context);
  });
});


//GET Create User ADMIN
app.get('/user/new/', is_admin_authenticated, function (req, res) {
  
  context = {
    user: req.user,
  }

  res.render('new_user', context);
});


//POST Create User ADMIN
app.post('/user/new/', is_admin_authenticated, function (req, res) {

  new_user = (req.body)
  new_user.phones = [req.body.phone]

  request({
    method: 'POST',
    uri: url_api + 'users/',
    json: {
      'user': new_user
    },
  }, function (error, response, body) {
    if (error || response.statusCode != 201 ) {
      console.log('Bad:', body);
      return res.redirect('/app/user/new')
    }

    console.log('Everything good', body);
    return res.redirect('/app/users/')
  });
});


//GET User by id ADMIN
app.get('/user/:id?', is_admin_authenticated, function (req, res) {
  
  var id = req.params.id;

  if (req.user._id == id) {
    return res.redirect('/app/profile/')
  }

  request({
    method: 'GET',
    uri: url_api + 'user/' + id,
    gzip: true
  }, function (error, response, body) {
      if (error) {
        return res.send('Error');
      }

      var user_now = (JSON.parse(body).user)

      var first_day = new Date(user_now.time);
      user_now.first_day = first_day.getHours() + ":" + first_day.getMinutes() + " | " + first_day.getDate() + "/" + first_day.getMonth() + "/" + first_day.getFullYear()

      context = {
        user_now: user_now,
        user: req.user,
        admin: (req.user.type == 0),
      }

      res.render('user', context);
  });
});

// GET change users ADMIN
app.get('/user_change/:id?', is_authenticated, function (req, res) {
  
  var id = req.params.id;

  request({
    method: 'GET',
    uri: url_api + 'user/' + id,
    gzip: true
  }, function (error, response, body) {
      if (error) {
        return res.send('Error');
      }

      var user_now = (JSON.parse(body).user)


      context = {
        user_now: user_now,
        user: req.user,
        admin: (req.user.type == 0),
      }

      res.render('user_change', context);
  });
});

//POST Change User ADMIN
app.post('/user_change/:id?', is_authenticated, function (req, res) {

  request_user = (req.body)

  request({
    method: 'PUT',
    uri: url_api + 'user_change/' + request_user.id,
    json: {
      'user': request_user
    },
  }, function (error, response, body) {
    if (error || response.statusCode != 200 ) {
      console.log('Bad:', body);
      return res.redirect('/app/user_change/' + request_user.id)
    }

    console.log('Everything good', body);
    return res.redirect('/app/user/' + request_user.id)
  });
});

//POST Activated toggle ADMIN
app.post('/user/:id?/activated_toggle', is_admin_authenticated, function (req, res) {

  var id = req.params.id;

  request({
    method: 'PUT',
    uri: url_api + 'user/' + id + '/activated_toggle',
  }, function (error, response, body) {
    if (error || response.statusCode != 200 ) {
      console.log('Bad:', body);
      return res.redirect('/app/user/' + id)
    }

    console.log('Everything good', body);
    return res.redirect('/app/user/' + id)
  });
});


//POST Delete User ADMIN
app.post('/user/:id?', is_admin_authenticated, function (req, res) {
  
  var id = req.params.id;

  request({
    method: 'DELETE',
    uri: url_api + 'user/' + id,
    gzip: true
  }, function (error, response, body) {
      if (error) {
        return res.send('Error');
      }
      return res.redirect('/app/users/?stu=23')
  });
});


/**
  Collection
**/
//GET Collections BOTH
app.get('/collections/', is_authenticated, function (req, res) {
  request(
  {
    method: 'GET',
    uri: url_api + 'collections/',
    gzip: true
  }, function (error, response, body){
      if (error)
      {
        return res.send('Error');
      }

      context = {
        user: (JSON.parse(body).user),
        admin: (req.user.type == 0),
        collections: JSON.parse(body).collections,
      }

      res.render('collections', context);
  });
});


//GET Create Collection ADMIN
app.get('/collection/new/', is_admin_authenticated, function (req, res) {

  context = {
    user: (req.user),
    admin: (req.user.type == 0),
  }

  res.render('new_collection', context);
});


//POST Create Collection ADMIN
app.post('/collection/new/', is_admin_authenticated, function (req, res) {

  new_collection = (req.body)

  request({
    method: 'POST',
    uri: url_api + 'collections/',
    json: {
      'collection': new_collection
    },
  }, function (error, response, body) {
    if (error || response.statusCode != 201 ) {
      console.log('Bad:', body);
      return res.redirect('/app/collection/new')
    }

    console.log('Everything good', body);
    return res.redirect('/app/collections/')
  });
});


app.get('/collection/:id?/', is_authenticated, function (req, res) {

  var id = req.params.id;

  request({
    method: 'GET',
    uri: url_api + 'collection_by_id/' + id,
    gzip: true
  }, function (error, response, body) {
      if (error) {
        return res.send('Error');
      }

      context = {
        user: (req.user),
        admin: (req.user.type == 0),
        collection: JSON.parse(body).collection,
      }

      res.render('collection', context);
  });

});


//GET Search Collections BOTH
app.get('/collections/search', is_authenticated, function (req, res){
  var search = req.param('search');
  request(
  {
    method: 'GET',
    uri: url_api + 'collections/search/' + search,
    gzip: true
  }, function (error, response, body){
      if (error)
      {
        return res.send('Error');
      }

      context = {
        user: (JSON.parse(body).user),
        admin: (req.user.type == 0),
        collections: JSON.parse(body).collections,
        searched: true,
      }

      res.render('collections', context)
  });

});


/**
  Arduino
**/
//GET Arduinos BOTH
app.get('/arduinos/', is_authenticated, function (req, res){
  request({
    method: 'GET',
    uri: url_api + 'arduinos/',
    gzip: true
  }, function (error, response, body){
      if (error)
      {
        return res.send('Error');
      }

      context = {
        user: (JSON.parse(body).user),
        admin: (req.user.type == 0),
        arduinos: JSON.parse(body).arduinos
      }

      res.render('arduinos', context);
  });
});


//GET Arduinos by Id BOTH
app.get('/arduinos/arduino/:id?', is_authenticated, function (req, res){
  
  var id = req.params.id;

  request({
    method: 'GET',
    uri: url_api + 'arduino_by_id/' + id,
    gzip: true
  }, function (error, response, body){
      if (error)
      {
        return res.send('Error');
      }

      arduino = JSON.parse(body).arduino

      console.log(arduino)

      for (var i = 0; i < arduino.temperatures.length; i++) {
        var date = new Date(arduino.temperatures[i].time);
        arduino.temperatures[i].date = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " | " + date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear()
        arduino.humidities[i].date = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " | " + date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear()
      }

      context = {
        user: (JSON.parse(body).user),
        admin: (req.user.type == 0),
        arduino: arduino,
      }

      res.render('arduino', context);
  });
});


//GET Arduinos change ADMIN
app.get('/arduinos/arduino_change/:id?', is_admin_authenticated, function (req, res){
  
  var id = req.params.id;

  request({
    method: 'GET',
    uri: url_api + 'arduino_by_id/' + id,
    gzip: true
  }, function (error, response, body){
      if (error)
      {
        return res.send('Error');
      }

      arduino = JSON.parse(body).arduino


      request({
        method: 'GET',
        uri: url_api + 'collections/',
        gzip: true
      }, function (error, response, body){
          if (error)
          {
            return res.send('Error');
          }

          context = {
            user: req.user,
            collections: JSON.parse(body).collections,
            admin: (req.user.type == 0),
            arduino: arduino,
          }

          res.render('arduino_change', context);
      });
      
  });
});


//POST Change arduino ADMIN
app.post('/arduino_change/:id?', is_admin_authenticated, function (req, res) {

  var id = req.params.id;

  request({
    method: 'PUT',
    uri: url_api + 'arduino_change/' + id,
    json: {
      'arduino': (req.body)
    },
  }, function (error, response, body) {
    if (error || response.statusCode != 201 ) {
      console.log('Bad:', body);
      return res.redirect('/app/arduino_change/' + id)
    }

    console.log('Everything good', body);
    return res.redirect('/app/arduinos/arduino/' + id)
  });
});


//GET add responsable user to arduino ADMIN
app.get('/arduinos/arduino/:id?/responsable_user', is_admin_authenticated, function (req, res){
  var arduino = req.params.id;

  request({
    method: 'GET',
    uri: url_api + 'user/',
    gzip: true
  }, function (error, response, body) {
      if (error) {
        return res.send('Error');
      }

      context = {
        admin: (req.user.type == 0),
        arduino: arduino,
        user: req.user,
        users: (JSON.parse(body).users),
      };

      res.render('add_responsable_user', context);
  });
});


//POST add responsable user to arduino ADMIN
app.post('/arduinos/arduino/:id?/responsable_user', is_admin_authenticated, function (req, res){
  
  var arduino_id = req.params.id;
  var new_responsable = (req.body);

  request({
    method: 'PUT',
    uri: url_api + 'arduino_user/',
    json: new_responsable,
  }, function (error, response, body) {
    if (error || response.statusCode != 200 ) {
      console.log('Bad:', body);
      return res.redirect('/app/arduinos/arduino/' + arduino_id + '/responsable_user')
    }

    console.log('Everything good', body);
    return res.redirect('/app/arduinos/')
  });
});


//GET Create Arduino ADMIN
app.get('/arduino/new/', is_admin_authenticated, function (req, res) {
  
  request(
  {
    method: 'GET',
    uri: url_api + 'collections/',
    gzip: true
  }, function (error, response, body){
      if (error)
      {
        return res.send('Error');
      }

      context = {
        user: req.user,
        collections: JSON.parse(body).collections,
      }

      res.render('new_arduino', context);
  });
});


//POST Create Arduino ADMIN
app.post('/arduino/new/', is_admin_authenticated, function (req, res) {

  new_arduino = (req.body);

  request({
    method: 'POST',
    uri: url_api + 'arduinos/',
    json: {
      'arduino': new_arduino
    },
  }, function (error, response, body) {
    if (error || response.statusCode != 201 ) {
      console.log('Bad:', body);
      return res.redirect('/app/arduino/new')
    }

    console.log('Everything good', body);
    return res.redirect('/app/arduinos/')
  });
});


app.get('*', function(req, res){
  res
    .status(404)
    .send('Not found');
});

app.post('*', function(req, res){
  res
    .status(404)
    .send('Not found');
});

app.put('*', function(req, res){
  res
    .status(404)
    .send('Not found');
});

app.delete('*', function(req, res){
  res
    .status(404)
    .send('Not found');
});


module.exports = app;