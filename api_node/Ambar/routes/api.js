/*
	Requires
*/
var express = require('express');
var app = express();
var async = require('async');
var bodyParser = require('body-parser');
var module_app;
var io;
var crypto = require('crypto');

async.waterfall([
	function (callback){
		module_app = require('../index');
		callback(null, module_app);
	},
	function (module_app1, callback){
		io = module_app.io;
	}
], function (err){
	console.log(err);
});
/*
	Middleware
*/
app.use(bodyParser.json('application/json'))

/**
	DB
**/
var login = require('../Login/loggin')
var encrypt = require('../encrypt/encrypt')

var User = require('../lib/users/model');
var Piece = require('../lib/pieces/model');
var Collection = require('../lib/collections/model');
var Arduino = require('../lib/arduinos/model');
var Password = require('../lib/password/model');
/*
	Routes
*/

function encriptar(user, pass) {
   var hmac = crypto.createHmac('sha1', user).update(pass).digest('hex')
   return hmac
}



//POST Users
app.post('/users/', function (req, res){
	if (typeof((req.body.user)) != "undefined")
	{
		var ReqUsername = req.body.user.username;
		var ReqEmail = req.body.user.email;
		var ReqPassword = req.body.user.password;
		var ReqName = req.body.user.name;
		var ReqType = req.body.user.type;
		var ReqPhone = req.body.user.phone;
		var ReqSurname = req.body.user.surname;
		var dateNow = new Date();
		var timeNow = dateNow.getTime()
		if ((typeof(ReqUsername) != "undefined" && typeof(ReqEmail) != "undefined" && typeof(ReqPassword) != "undefined" && typeof(ReqName) != "undefined" && typeof(ReqSurname) != "undefined")
			&& (ReqUsername != "" && ReqEmail != "" && ReqPassword != "" && ReqName != "" && ReqSurname != ""))
		{
			async.waterfall([
				function (callback){
					if (typeof(ReqType) == "undefined") {
						ReqType = 1;
					}
					callback(null, module_app);
				},
				function (module_app1, callback){
					var NewPassword = encriptar(ReqUsername, ReqPassword);

					var newUser = new User({ 
						username: ReqUsername, 
						email: ReqEmail, 
						password: NewPassword, 
						name: ReqName,
						type: ReqType, 
						surname: ReqSurname, 
						phones: [ReqPhone],
						time: timeNow,
						activated: true,
					});
					
					newUser.save(function (err) {
						if (err)
						{
							console.log(err);
							res
								.status(500)
								.send("Database error");
						}
						else
						{
							return res
								.status(201)
								.json({
									user: {
										username: ReqUsername, 
										email: ReqEmail, 
										password: NewPassword, 
										name: ReqName,
										type: ReqType, 
										surname: ReqSurname, 
										phones: ReqPhone,
									}
								});	
						}
					});
				}
			], function (err){
				console.log(err);
			});
		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
			.status(400)
			.send("Your request is without user");
	}
});

//GET all users
app.get('/user/', function (req, res) {
	User
		.find({})
		.populate('arduinos')
		.exec(function (err, users){
			if (err) 
			{
				console.log(err);
				return res
						.status(500)
						.send({});
			}
			else
			{
				return res
					.status(200)
					.json({
						users : users
					})
			}
		});
});

//GET users by id
app.get('/user/:id?', function (req, res) {
	
	var id = req.params.id;
		
	if (!id){
		return res
			.status(400)
			.send('No id');
	}

	User
		.findById(id)
		.populate('arduinos')
		.exec(function (err, user){
			if (err) 
			{
				console.log(err);
				return res
						.status(400)
						.send({});
			}
			else
			{
				return res
						.status(200)
						.json({
							user : user
						});
			}
		});
});

//POST Login
app.post('/login/', function (req,res){

	if (typeof((req.body.user)) != "undefined")
	{
		var ReqEmail = req.body.user.email;
		var ReqPassword = req.body.user.password;

		if ((typeof(ReqEmail) != "undefined" && typeof(ReqPassword) != "undefined")
			&& (ReqEmail != "" && ReqPassword != ""))
		{
			login.login(ReqEmail, ReqPassword, function (found){
			
      			res
      				.status(202)
      				.json(found);
			});
		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
			.status(400)
			.send("Your request is without user");
	}
});

//PUT activated toggle
app.put('/user/:id?/activated_toggle', function (req, res){

	var user_id = req.params.id

	if ((typeof((user_id)) != "undefined")
		&& (user_id != ""))
	{
		User.findById(user_id, function (err, user_now){
			activated_now = user_now.activated

			User.findByIdAndUpdate( user_id,
			{"activated": (activated_now == false) },		 
			{safe: true, upsert: true}, function (err, user){
				if (err) 
				{
					console.log(err);
					return res
							.status(400)
							.send({});
				}
				else
				{
					
					return res
						.status(200)
						.json({
							user: user,
						});
				}
			});
		});
	}
	else
	{
		return res
			.status(400)
			.send("Your request is wrong");
	}
});

app.put('/user_change/:id?', function (req, res) {
	if (typeof((req.body.user)) != "undefined")
	{
		var ReqEmail = req.body.user.email;
		var ReqName = req.body.user.name;
		var ReqPhone = req.body.user.phone;
		var ReqSurname = req.body.user.surname;
		var id = req.params.id;
		if ((typeof(ReqEmail) != "undefined" && typeof(ReqName) != "undefined" && typeof(ReqSurname) != "undefined")
			&& (ReqEmail != "" && ReqName != "" && ReqSurname != ""))
		{
			User.findByIdAndUpdate( id,
			{"email": ReqEmail, "name": ReqName, "phones": [ReqPhone], "surname": ReqSurname},		 
			{safe: true, upsert: true}, function (err, user){
				if (err) 
				{
					console.log(err);
					return res
							.status(400)
							.send({});
				}
				else
				{
					
					return res
						.status(200)
						.json({
							user: user,
						});
				}
			});
		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
				.status(400)
				.send("Your request is wrong");
	}
});

//DELETE Users
app.delete('/user/:id?', function (req, res){

	var id = req.params.id;

	if (!id){
		return res
			.status(400)
			.send('No id');
	}

	User.findByIdAndRemove(id, function (err, user){
		if (err) 
		{
			console.log(err);
			return res
					.status(400)
					.send("User does not exist");
		}
		else
		{
			try
			{
				var user_arduinos = user.arduinos;
				async.waterfall([
					function remove_responsableUsers(callback)
					{
						for(var i = 0; i < user_arduinos.length; ++i)
						{
							Arduino.findById(user_arduinos[i], function (err, arduino){
								
								for (var j = 0; j < arduino.responsableUsers.length; ++j)
								{
									if(arduino.responsableUsers[j].toString() == user._id.toString())
									{
										arduino.responsableUsers.splice(j, 1);
										arduino.save()
										break;
									}
								}
							});
						}
						callback(null);
					},
					function (callback)
					{
						return res
							.status(204)
							.send("Removed");
					}
				], function (err){
					console.log(err);
				});
			}
			catch(err)
			{
				return res
					.status(404)
					.send("Not Found");
			}
		}
	})	
});

//POST arduinos
app.post('/arduinos/', function (req, res){
	
	if (typeof((req.body.arduino)) != "undefined")
	{
		var ReqArduino = req.body.arduino.id_arduino;
		var ReqCollection = req.body.arduino.collection;
		
		if ((typeof(ReqArduino) != "undefined" && typeof(ReqCollection) != "undefined")
			&& (ReqArduino != "" && ReqCollection != ""))
		{
			var newArduino = new Arduino({ 
				collections : [ReqCollection], 
				id_arduino: ReqArduino
			});
			newArduino.save(function (err) {
				if (err)
				{
					console.log(err);
					res
						.status(400)
						.send("DataBase error");
				}
				else
				{
					Collection.findByIdAndUpdate( ReqCollection,
			    
				    {$push: {"arduinos": newArduino._id}},
				    
				    {safe: true, upsert: true}, function (err, collection) {

				    	console.log('lalala')

						return res
							.status(201)
							.json({
								arduino: {
									collections : [ReqCollection],
									id_arduino : ReqArduino
								}
							});	
					});
				}
			});
		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
			.status(400)
			.send("Your request is without piece");
	}
});

//GET arduino by id
app.get('/arduino_by_id/:id?', function (req, res){

	var id = req.params.id;
		
	if (!id){
		return res
			.status(400)
			.send('No id');
	}

	Arduino
		.findById(id)
		.populate('responsableUsers')
		.populate('pieces')
		.populate('collections')
		.exec(function ( err, arduino){
			if (err) 
			{
				console.log(err);
				return res
						.status(400)
						.send({});
			}
			else
			{
				return res
						.status(200)
						.json({
							arduino : arduino
						});
			}
		});
});

//PUT change arduino
app.put('/arduino_change/:id?', function (req, res) {
	if (typeof((req.body.arduino)) != "undefined")
	{
		var id = req.params.id;
		var ReqArduino = req.body.arduino.id_arduino;
		var ReqCollection = req.body.arduino.collection;
		
		if ((typeof(ReqArduino) != "undefined" && typeof(ReqCollection) != "undefined")
			&& (ReqArduino != "" && ReqCollection != ""))
		{
			Arduino.findById(id, function (err, arduino1) {
				var collection_before = arduino1.collections[0]
				if (arduino1.id_arduino == ReqArduino) {
					Arduino.findByIdAndUpdate(id,
					{"collections": [ReqCollection]}, function (err, arduino) {
						if (err)
						{
							console.log(err);
							res
								.status(400)
								.send("DataBase error");
						}
						else
						{
							Collection.findByIdAndUpdate( ReqCollection,
						    {$push: {"arduinos": id}},
						    {safe: true, upsert: true}, function (err, collection) {

						    	console.log(collection)
						    	console.log('collection')

						    	Collection.findById(collection_before,
			                    function (err, collection2) {

			                    	console.log(collection2);
			                    	console.log('collection2');

			                    	collection2.arduinos.remove(id)
			                    	collection2.save(function (err) {
				                        return res
												.status(201)
												.json({
													arduino: {
														collections : [ReqCollection],
														id_arduino : ReqArduino
													}
												});
			                    	})
			                    });
							});
						}
					});
				} else {
					Arduino.findByIdAndUpdate(id,
					{"id_arduino": ReqArduino, "collections": [ReqCollection]}, function (err, arduino) {
						if (err)
						{
							console.log(err);
							res
								.status(400)
								.send("DataBase error");
						}
						else
						{
							Collection.findByIdAndUpdate( ReqCollection,
						    {$push: {"arduinos": id}},
						    {safe: true, upsert: true}, function (err, collection) {

						    	console.log(collection)
						    	console.log('collection')

						    	Collection.findById(collection_before,
			                    function (err, collection2) {
			                    	console.log(collection2);
			                    	console.log('collection2');
			                    	collection2.arduinos.remove(id)
			                    	collection2.save(function (err) {
				                        return res
												.status(201)
												.json({
													arduino: {
														collections : [ReqCollection],
														id_arduino : ReqArduino
													}
												});
			                    	})
			                    });
							});
						}
					});
				}
			});
		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
			.status(400)
			.send("Your request is without piece");
	}

});


//GET all arduinos
app.get('/arduinos/', function (req, res){

	Arduino
		.find({})
		.populate('collections')
		.exec(function (err, arduinos){
			if (err) 
			{
				console.log(err);
				return res
						.status(500)
						.send({});
			}
			else
			{
				var allArduinos = [];

				arduinos.forEach(function (arduino){
					allArduinos.push(arduino)
				});
				return res
					.status(200)
					.json({
						arduinos : allArduinos
					})
			}
		});
});

//POST Collections
app.post('/collections/', function (req, res){
	
	if (typeof((req.body.collection)) != "undefined")
	{
		var ReqName = req.body.collection.name;
		
		if ((typeof(ReqName) != "undefined")
			&& (ReqName != "" ))
		{
			var NewCollection = new Collection({
				name : ReqName
			});
			NewCollection.save(function (err){
				if (err)
				{
					console.log(err);
					res
						.status(400)
						.send("You need more");
				}
				else
				{
					return res
						.status(201)
						.json({
							collection: {
								name : ReqName
							}
						});	
				}
			});

		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
			.status(400)
			.send("Your request is without collection");
	}
});

//GET collection by id
app.get('/collection_by_id/:id?', function (req, res){
	
	var id = req.params.id;
		
	if (!id){
		return res
			.status(400)
			.send('No id');
	}

	Collection
		.findById(id)
		.populate('arduinos')
		.exec(function ( err, collection){
		if (err) 
		{
			console.log(err);
			return res
					.status(400)
					.send({});
		}
		else
		{
			console.log(collection)
			console.log('lala')
			return res
					.status(200)
					.json({
						collection : collection
					});
		}
	});
});

//GET all collections
app.get('/collections/', function (req, res){

	Collection
		.find({})
		.exec(function (err, collections){
			if (err) 
			{
				console.log(err);
				return res
						.status(400)
						.send({});
			}
			else
			{
				var allCollections = [];

				collections.forEach(function (collection){
					allCollections.push(collection);
				});

				return res
					.status(200)
					.json({
						collections : allCollections
					})
			}
		});
});

//GET search collections
app.get('/collections/search/:search?', function (req, res){
	var search =req.params.search;

	Collection.find({name : {$regex: search, $options:'i'}}, function (err, collections){
		if (err)
		{
			console.log(err);
				return res
						.status(400)
						.send({});
		}
		else
		{
			return res 
					.status(200)
					.json({
						collections : collections
					})
		}
	});
})

//PUT collections in arduinos
app.put('/arduino_collection/', function (req, res){

	var ReqArduino = req.body.arduinoId;
	var ReqCollection = req.body.collectionId;
	if ((typeof((ReqArduino)) != "undefined" && typeof((ReqCollection)) != "undefined") 
		&& (ReqArduino != "" && ReqCollection != ""))
	{
		Collection.findById( ReqCollection, function (err, collection){
			if (err) 
			{
				console.log(err);
				return res
						.status(400)
						.send({});
			}
			else
			{
				Arduino.findByIdAndUpdate( ReqArduino,
			    
				    {$push: {"collections": collection._id}},
				    
				    {safe: true, upsert: true}, function (err, arduino) {
				        if (err)
						{
							console.log(err);
							res
								.status(500)
								.send("Arduino id does not exist");
						}
						else
						{
							return res
								.status(200)
								.json({
									arduino: {
										collections : arduino.collections
									}
								});	
						}
				    });
			}
		});
	}
	else
	{
		return res
			.status(400)
			.send("Your request is wrong");
	}
});

// DELETE collection
app.delete('/collection/:id?', function (req, res){

	var id = req.params.id;

	if (!id){
		return res
			.status(400)
			.send('No id');
	}
	Collection.findByIdAndRemove(id, function (err){
		if (err) 
		{
			console.log(err);
			return res
					.status(400)
					.send("Collection id does not exist");
		}
		else
		{
			return res
					.status(204)
					.send("Removed");
		}
	})	
});

//POST Pieces
app.post('/pieces/', function (req, res){
	
	if (typeof((req.body.piece)) != "undefined")
	{
		var ReqName = req.body.piece.name;
		var ReqRegistryId = req.body.piece.registryId;
		
		if ((typeof(ReqName) != "undefined" && typeof(ReqRegistryId) != "undefined")
			&& (ReqName != "" && ReqRegistryId != "" ))
		{
			var NewPiece = new Piece({
				name : ReqName,
				registryId : ReqRegistryId
			});
			NewPiece.save(function (err){
				if (err)
				{
					console.log(err);
					res
						.status(400)
						.send("You need more");
				}
				else
				{
					return res
						.status(201)
						.json({
							piece: {
								name : ReqName,
								registryId : ReqRegistryId
							}
						});	
				}
			});

		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
			.status(400)
			.send("Your request is without piece");
	}
});

//GET piece by id
app.get('/piece_by_id/:id?', function (req, res){
	
	var id = req.params.id;
		
	if (!id){
		return res
			.status(400)
			.send('No id');
	}

	Piece.findById( id, function (err, piece){
		if (err) 
		{
			console.log(err);
			return res
					.status(400)
					.send({});
		}
		else
		{
			return res
					.status(200)
					.json({
						piece : piece
					});
		}
	});
});

//GET all pieces
app.get('/pieces/', function (req, res){
	
	Piece.find({}, function (err, pieces) {
		if (err) 
		{
			console.log(err);
			return res
					.status(400)
					.send({});
		}
		else
		{
			var allPieces = [];

			pieces.forEach(function (piece){
				allPieces.push(piece);
			});

			return res
				.status(200)
				.json({
					pieces : allPieces
				})
		}
	});
});

//PUT pieces in arduinos
app.put('/arduino_piece/', function (req, res){

	var ReqArduino = req.body.arduinoId;
	var ReqPiece = req.body.pieceId;
	if ((typeof((ReqArduino)) != "undefined" && typeof((ReqPiece)) != "undefined") 
		&& (ReqArduino != "" && ReqPiece != ""))
	{
		Piece.findByIdAndUpdate( ReqPiece,
		{"arduino" : ReqArduino}, {safe: true, upsert: true}, 
		function (err, piece){
			if (err) 
			{
				console.log(err);
				return res
						.status(400)
						.send({});
			}
			else
			{
				Arduino.findByIdAndUpdate( ReqArduino,
			    
				    {$push: {"pieces": piece._id}},
				    
				    {safe: true, upsert: true}, function (err, arduino) {
				        if (err)
						{
							console.log(err);
							res
								.status(500)
								.send("arduino does not exist");
						}
						else
						{
							return res
								.status(200)
								.json({
									arduino: {
										pieces : arduino.pieces
									},
									piece: {
										arduino : piece.arduino
									}
								});	
						}
				    });
			}
		});
	}
	else
	{
		return res
			.status(400)
			.send("Your request is wrong");
	}
});

// DELETE piece
app.delete('/piece/:id?', function (req, res){

	var id = req.params.id;

	if (!id){
		return res
			.status(400)
			.send('No id');
	}
	Piece.findByIdAndRemove(id, function (err){
		if (err) 
		{
			console.log(err);
			return res
					.status(400)
					.send("Piece id does not exist");
		}
		else
		{
			return res
					.status(204)
					.send("Removed");
		}
	})	
});

//POST Humidity / temperature
app.post('/arduinos/record/', function (req, res){
	
	if (typeof((req.body.arduino)) != "undefined")
	{
		var ReqId_arduino = req.body.arduino.id_arduino;
		var ReqHumidity = req.body.arduino.humidity;
		var ReqTemperature = req.body.arduino.temperature;
		
		if ((typeof(ReqId_arduino) != "undefined" && typeof(ReqHumidity) != "undefined" && typeof(ReqTemperature) != "undefined" )
			&& (ReqId_arduino != "" && ReqHumidity != "" && ReqTemperature != ""))
		{
			var dateNow = new Date();
			var timeNow = dateNow.getTime()

			io.emit('send_record', {
				temperature: ReqTemperature,
				humidity: ReqHumidity,
				time: timeNow,
				id_arduino: ReqId_arduino
			});
				
			io.emit('send_record ' + ReqId_arduino, {
				temperature: ReqTemperature,
				humidity: ReqHumidity,
				time: timeNow,
				id_arduino: ReqId_arduino
			});

			var NewHumidity = {
				humidity : ReqHumidity,
				time : timeNow
			};

			var NewTemperature = {
				temperature : ReqTemperature,
				time : timeNow
			};

			Arduino.findOneAndUpdate( {"id_arduino" : ReqId_arduino},
			    
			    {$push: {"humidities": NewHumidity}},
			    
			    {safe: true, upsert: true}, function (err, arduino) {
			        if (err)
					{
						console.log(err);
						res
							.status(400)
							.send("Id does not exist");
					}
					else
					{
						Arduino.findOneAndUpdate( {"id_arduino" : ReqId_arduino},
			    
						    {$push: {"temperatures": NewTemperature}},
						    
						    {safe: true, upsert: true}, function (err, arduino) {
						        if (err)
								{
									console.log(err);
									res
										.status(500)
										.send("Database error");
								}
								else
								{
									return res
										.status(201)
										.json({
											temperature: {
												temperature : ReqTemperature,
												time : timeNow
											},
											humidity: 
											{
												humidity : ReqHumidity,
												time: timeNow
											}
										});	
								}
						    }
						);	
					}
			    }
			);
			

		}
		else
		{
			return res
				.status(400)
				.send("Your request is wrong");
		}
	}
	else
	{
		return res
			.status(400)
			.send("Your request is without arduino");
	}
});

//PUT responsable user
app.put('/arduino_user/', function (req, res){

	var ReqArduino = req.body.arduinoId;
	var ReqUser = req.body.userId;

	if ((typeof((ReqArduino)) != "undefined" && typeof((ReqUser)) != "undefined")
		&& (ReqArduino != "" && ReqUser != ""))
	{
		User.findById(ReqUser, function (err, user_id){
			for(var i = 0; i < user_id.arduinos.length; i++) {
				if(user_id.arduinos[i] == ReqArduino) {
					return res
						.status(400)
						.send("Is already registered");
				}
			}
			console.log(ReqArduino)
			User.findByIdAndUpdate( ReqUser,
			{$push: {"arduinos": ReqArduino }},		 
			{safe: true, upsert: true}, function (err, user){
				if (err) 
				{
					console.log(err);
					return res
							.status(400)
							.send({});
				}
				else
				{
					Arduino.findByIdAndUpdate( ReqArduino,
				    
					    {$push: {"responsableUsers": user._id}},
					    
					    {safe: true, upsert: true}, function (err, arduino) {
					        if (err)
							{
								console.log(err);
								res
									.status(500)
									.send("Piece does not exist");
							}
							else
							{
								return res
									.status(200)
									.json({
										arduino: {
											responsableUsers : arduino.responsableUsers
										},
										user: {
											arduinos : user.arduinos
										}
									});	
							}
					    });
				}
			});
		});
	}
	else
	{
		return res
			.status(400)
			.send("Your request is wrong");
	}
});

app.get('*', function(req, res){
  res
  	.status(404)
  	.send('Not found');
});

app.post('*', function(req, res){
  res
  	.status(404)
  	.send('Not found');
});

app.put('*', function(req, res){
  res
  	.status(404)
  	.send('Not found');
});

app.delete('*', function(req, res){
  res
  	.status(404)
  	.send('Not found');
});

module.exports = app;