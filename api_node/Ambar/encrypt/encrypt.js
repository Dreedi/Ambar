
var Passw = require('../lib/password/model');
var crypto = require('crypto');

function encrypt(passwordNow, callback)
{
    Passw.findOne({type: 'Algo'}, function (err, passwordAlgo){
        if (!(err))
        {
            var hmac = crypto.createHmac('sha1', passwordAlgo.password).update(passwordNow).digest('hex');
            callback(hmac);
        }
        else
        {
            callback(err);    
        }
    });
}

exports.encrypt = encrypt

exports.General_API = function (passwordNow, callback){

    encrypt(passwordNow, function (result)
    {
        Passw.findOne({type: "Gen", password: result}, function(err, password){
            if(password != null)
            {
                callback(true);
            }
            else
            {
                callback(false);
            }
        });
    });
};