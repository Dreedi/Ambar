import serial
import requests
from time import gmtime, strftime
import requests

def searchArduinos():
    arduinos = []
    for i in range(4):
        port = '/dev/ttyACM' + str(i)
        try:
            arduino = serial.Serial(port, baudrate=9600)
            arduinos.append(arduino)
            print "Arduino founded at port ", port
        except:
            print "Arduino doesnt found at port: ", port    
    return arduinos

def parseData(arduinos):
    url = 'http://pablotrinidad.ngrok.com/api/post/record/'
    for arduino in arduinos:
        message = arduino.readline()[:-1]
        if validateMessage(message):
            TemperatureAndHumidityAndArduinoID = getTemperatureAndHumidityAndArduinoID(message)
            outputData = {
                "temperature": TemperatureAndHumidityAndArduinoID[0],
                "humidity": TemperatureAndHumidityAndArduinoID[1],
                "arduino": TemperatureAndHumidityAndArduinoID[2],
            }
            r = requests.post(url, outputData)
            print r.status_code
            

def validateMessage(message):
    valid = False
    if '/' in message and '-' in message and len(message) >= 23:
        
        slash = 0
        dash = 0
        
        for i in message:
            if i == '/':
                slash += 1
            elif i == '-':
                dash += 1
                
        if slash == 1 and dash == 1:
            valid = True
            
        else:
            valid = False
            
    else:
        valid = False
    return valid

def getTemperatureAndHumidityAndArduinoID(message):
    dashPosition = message.index('-')
    slashPosition = message.index('/')
    
    humidity = message[slashPosition+1:dashPosition]
    temperature = message[dashPosition+1:-18]
    idArduino = message[dashPosition+4:-2]
    temperatureAndHumidity = [temperature, humidity, idArduino]
    return temperatureAndHumidity
    
    
arduinos = searchArduinos()
while True:
    parseData(arduinos)
    
    
    
    
    
    
    
    